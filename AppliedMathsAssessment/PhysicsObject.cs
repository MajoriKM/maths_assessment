﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AppliedMathsAssessment
{
    public class PhysicsObject
    {
        // ------------------
        // Data
        // ------------------

        // Transform
        protected Vector3 rotation;
        protected Vector3 position;
        protected Vector3 scale = Vector3.One;

        // Physics
        protected BoundingBox hitBox;
        protected Vector3 velocity;
        protected Vector3 acceleration;
        protected float drag = 0.05f;
        protected Vector3 collisionScale = Vector3.One;
        protected bool isStatic = false;    // Not moved by physics
        protected bool useGravity = false;  // Falls with gravity each frame
        protected bool isTrigger = true;    // Does not trigger physics affects (but can still be sensed with collisions)
        protected float gravityScale = 1f;

        // Previous state
        protected Vector3 positionPrev;
        protected Vector3 velocityPrev;
        protected Vector3 accelerationPrev;

        // Numerical Integration
        public enum IntegrationMethod {
            EXPLICIT_EULER,
            METHOD_TWO,
            METHOD_THREE
        };
        private IntegrationMethod currentIntegrationMethod = IntegrationMethod.EXPLICIT_EULER; // UPDATE THIS TO TEST TASK 6

        // ------------------
        // Behaviour
        // ------------------
        public BoundingBox GetHitBox()
        {
            return hitBox;
        }
        // ------------------
        public virtual void UpdateHitBox()
        {
            // Just make a cube hitbox based on the scale
            hitBox = new BoundingBox(-collisionScale * 0.5f, collisionScale * 0.5f);

            // Move to correct position in game world
            hitBox.Min += position;
            hitBox.Max += position;
        }
        // ------------------
        public Vector3 GetPosition()
        {
            return position;
        }
        // ------------------
        public void SetPosition(Vector3 newPosition)
        {
            position = newPosition;
        }
        // ------------------
        public void SetScale(Vector3 newScale)
        {
            scale = newScale;
        }
        // ------------------
        public Vector3 GetGravityVector()
        {
            return new Vector3(0, -9.8f * gravityScale, 0);
        }
        // ------------------
        public virtual void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Update acceleration due to gravity
            if (useGravity)
                acceleration.Y = -9.8f * gravityScale;

            // Store current state before making any modifications
            Vector3 positionCur = position;
            Vector3 velocityCur = velocity;
            Vector3 accelerationCur = acceleration;

            // Update velocity due to drag
            velocity *= (1.0f - drag);

            // Update velocity and position based on acceleration
            // Uses numerical integration (multiple possible methods)
            switch (currentIntegrationMethod)
            {
                case IntegrationMethod.EXPLICIT_EULER:
                    // This method is being deprecated due to stability issues.
                    velocity += acceleration * dt;
                    position += velocity * dt;
                    break;
                    
                ///////////////////////////////////////////////////////////////////
                //
                // CODE FOR TASK 6 SHOULD BE ENTERED HERE
                //
                ///////////////////////////////////////////////////////////////////
                case IntegrationMethod.METHOD_TWO:

                    // Insert method two code here
                    // Update the enum to name the method being used


                    break;

                case IntegrationMethod.METHOD_THREE:

                    // Insert method three code here
                    // Update the enum to name the method being used


                    break;

                ///////////////////////////////////////////////////////////////////  
                // END TASK 6 CODE
                ///////////////////////////////////////////////////////////////////  
            }

            // Store current state as previous state
            positionPrev = positionCur;
            velocityPrev = velocityCur;
            accelerationPrev = accelerationCur;

            // Update hitbox
            UpdateHitBox();
        }
        // ------------------
        public virtual void HandleCollision(PhysicsObject other)
        {
            // Don't react with physics if this object is static
            if (isStatic || isTrigger || other.isTrigger)
                return;

            ///////////////////////////////////////////////////////////////////
            //
            // CODE FOR TASK 3 SHOULD BE ENTERED HERE
            //
            ///////////////////////////////////////////////////////////////////

            // OLD VERSION, DEPRECATED
            // Re-write this to account for more realistic collisions using 
            // the incoming angle
            // If this wall intersects with our ball...
            
            // We'll need some vectors to keep track of our collision
            // We will need two axes on the colliding plane of our bounding box,
            // and the normal vector (perpendicular to the plane we collided with)
            Vector3 firstAxis, secondAxis, normal;

            // move back to stored position before moving into collision
            position = positionPrev;

            BoundingBox thisHitBox = GetHitBox();
            BoundingBox otherHitBox = other.GetHitBox();

            // Find three suitable corners, and make two vectors out
            // of them, representing a plane of collision.



            Vector3[] corners = otherHitBox.GetCorners();

            // If we are colliding in the X direction...
            if ((thisHitBox.Min.X - velocity.X) > otherHitBox.Max.X ||
            (thisHitBox.Max.X - velocity.X) < otherHitBox.Min.X)
            {
               // use the east plane for collision (east and west would be the same)
               firstAxis = corners[1] - corners[5]; // vector from the top right to the top left
               secondAxis = corners[6] - corners[5]; // vector from the top right bottom right
            }
            else // if we are colliding in the Y direction...
            {
                // use the south plane for collision (south and north would be the same)
                firstAxis = corners[1] - corners[0]; // vector from top left of the plane to the top right
                secondAxis = corners[3] - corners[0]; // vector from the top left of the plane to the bottom left
            }

            // Get a cross product between those two vectors to define a normal (perpendicular)
            // angle from that plane

            normal = Vector3.Cross(firstAxis, secondAxis);

            // reduce it to a unit vector (lenth 1)
            normal.Normalize();

            // and reflect the player's velocity off that surface using the normal.
            // This uses the dot product internally
            velocity = Vector3.Reflect(velocityPrev, normal);
            
        

        ///////////////////////////////////////////////////////////////////  
        // END TASK 3 CODE
        ///////////////////////////////////////////////////////////////////  
        }
        // ------------------
        public virtual void Draw(Camera cam, DirectionalLightSource light)
        {

        }
        // ------------------
    }
}
